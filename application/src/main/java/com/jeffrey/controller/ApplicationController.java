package com.jeffrey.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import java.util.Date;
import java.util.concurrent.atomic.AtomicLong;

@RestController
public class ApplicationController {

    @GetMapping("/checkStatus")
    public String checkStatus() {
        return "Server is up, current server time is: " + new Date();
    }


    @GetMapping("/")
    public RedirectView redirectOneSlash() {
        return new RedirectView("/swagger-ui.html");
    }

    @GetMapping("")
    public RedirectView redirectEmpty() {
        return new RedirectView("/swagger-ui.html");
    }
}
